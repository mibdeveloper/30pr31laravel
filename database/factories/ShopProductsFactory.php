<?php

use Faker\Generator as Faker;
use App\ShopProduct;

$factory->define(App\ShopProduct::class, function (Faker $faker) {

    /*
     * Schema::create('shop_products', function (Blueprint $table) {
            $table->string('name')->unique();
            $table->string('description');
            $table->integer('price');
            $table->string('image_url');
            $table->timestamps();
        });
     *
     * */
    return [
        'name' => $faker->sentence(2).uniqid(microtime(),true),
        'description' => $faker->sentence(15),
        'price' => $faker->randomNumber(4),
        'category_id' => 0,
        'image_url' => '',
    ];
});
