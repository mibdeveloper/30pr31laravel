<?php

use Illuminate\Database\Seeder;
use App\ShopCategory;

class ShopProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories = ShopCategory::where("parent_id" , ">", 0)->get();

        foreach($categories as $cat) {
            echo "Generating products for ".$cat->id."\n";
            factory(App\ShopProduct::class, 200)->create([
                'category_id' => $cat->id
            ]);
        }

    }
}
