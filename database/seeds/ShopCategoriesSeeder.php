<?php

use Illuminate\Database\Seeder;

class ShopCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\ShopCategory::class, 3)->create()->each(function ($c) {
            factory(App\ShopCategory::class, 5)->create([
                'parent_id' => $c->id
            ]);
        });
    }
}
