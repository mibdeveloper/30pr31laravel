<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('setlocale/{locale}', function ($locale) {

    if (in_array($locale, \Config::get('app.locales'))) {
# Проверяем, что у пользователя выбран доступный язык

        //echo "qwefqwegf $locale";
        Session::put('locale', $locale);
# И устанавливаем его в сессии под именем locale
    }

    return redirect()->back();
# Редиректим его <s>взад</s> на ту же страницу

});


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('shop-categories/create/{id}',
        'ShopCategoriesController@createCategory')
        ->name('voyager.shop-categories.create-category')
        ->middleware('admin.user');
});
