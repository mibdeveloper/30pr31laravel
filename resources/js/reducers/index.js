import { combineReducers } from 'redux';
import categoryReducer from './category'
import productReducer from './product'
import {  routerReducer } from 'react-router-redux'

export default combineReducers({
        categories:categoryReducer,
        products:productReducer,
        routing: routerReducer,
});
