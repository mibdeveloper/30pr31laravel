let initialState = {
    productsList:[],
    currentPage:1,
    productsOnPage:10
}


export default function productReducer(state = initialState, action) {

    //console.log("productReducer Action:" + action.type);
    //console.log("State:", state);
    switch(action.type){
        case 'UPDATE_PRODUCTS_LIST':{
            return {...state, productsList: [...action.payload], currentPage:1};
        }break;
        case 'PAGE_CHANGED':{
            return {...state, currentPage:action.payload};
        }break;
    }
    return state;
}
