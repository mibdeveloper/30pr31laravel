import {UPDATE_CATEGORIES_LIST} from './../constants/Category';

export default function categoryReducer(state = {}, action) {

    //console.log("Action:" + action.type);
    //console.log("State:", state);
    switch(action.type){
        case UPDATE_CATEGORIES_LIST:{
            console.log("categories updated");
            /*
            *
            * currentPage:1,
            * currentCategory:4,
            * categoryList:[.....]
            *
            * */

            return {...state, categoryList: [...action.payload]};
        }break;
        case 'UPDATE_CURRENT_CATEGORY':{
            return {...state, currentCategory: action.payload};
        }break;

    }
    return state;
}
