import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {connect} from "react-redux";
//import {bindActionsToCreators} from 'react-redux';

import {UPDATE_CATEGORIES_LIST} from './../constants/Category';

var classNames = require('classnames');

/* Main Component */
class Category extends Component {


    /*componentDidMount() is a lifecycle method
     * that gets called after the component is rendered
     */
    componentDidMount() {
        /* fetch API in action */

        fetch('/api/v1/categories')
            .then(response => {

                return response.json();
            })
            .then(categories => {
                this.props.onUpdateCategoiesList(categories);
            });

    }

    updateProductList(category){



        fetch('/api/v1/products?category=' + category.id)
            .then(response => {

                if(response.ok) {
                    response.json()
                        .then(products => {
                          this.props.onUpdateProductsList(category.id, products);
                    }).catch(error => { console.log('json convert error: ', error); });
                }
            }).catch(error => { console.log('request failed', error); });






    }

    renderCategories() {

       /* return (

        <div>Waiting for categories.</div>
        )*/

        //console.log("!!!!!!!!!",this.props);
        if(!this.props.categories) {
            return (
                <div>Waiting for categories.</div>
            );
        }

        return this.props.categories.map(category => {

            let itemClass = classNames({
                active: category.id == this.props.currentCategory,
                menuItem:true,
            });

            return (

                <li className={itemClass}
                    onClick={
                        () =>this.updateProductList(category)
                    }
                    key={category.id}>
                    { category.name }</li>
            );
        })
    }

    render() {
        /* Some css code has been removed for brevity */
        /* */

        return (


            <div>

                <ul>
                    { this.renderCategories() }
                </ul>
            </div>

        );
    }
}
//export default Category;

export default connect(
    state =>

        ({
            categories: state.categories.categoryList,
            currentCategory: state.categories.currentCategory,
        })

    ,
    dispatch => ({
        onUpdateCategoiesList:(list)=>{
            //console.log("Sending UPDATE_CATEGORIES_LIST",list);
            dispatch({type: UPDATE_CATEGORIES_LIST, payload: list});
        },
        onUpdateProductsList:(categoryId, list)=>{
            dispatch({type: 'UPDATE_CURRENT_CATEGORY', payload: categoryId});
            dispatch({type: 'UPDATE_PRODUCTS_LIST', payload: list});
        }
    })
)(Category);


/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";
*/

