import 'rc-pagination/assets/index.css';
import Pagination from 'rc-pagination';
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Category from './Category';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {connect} from 'react-redux';
import { Link } from 'react-router-dom'






/* Main Component */
class App extends Component {

    /*constructor() {

        super();
        //Initialize the state in the constructor
        this.state = {
            products: [],
        }
    }*/
    /*componentDidMount() is a lifecycle method
     * that gets called after the component is rendered
     */
    componentDidMount() {
        /* fetch API in action */
        fetch('/api/v1/products')
            .then(response => {

                return response.json();
            })
            .then(products => {
                //Fetched product is stored in the state
                this.setState({ products });
            });
    }

    renderProducts() {



        //console.log("Main.props",this.props);
        if(this.props.products && this.props.products.length) {

            let currentPageProducts = this.props.products.slice(

                /**/
                (this.props.currentPage - 1) * this.props.productsOnPage,
                this.props.productsOnPage * this.props.currentPage
            );
            //console.log(currentPageProducts);
            return   <ul>
                { currentPageProducts.map(product => {
                    // console.log(product);
                    return (
                        /* When using list you need to specify a key
                         * attribute that is unique for each list item
                        */
                        <li key={product.id}>
                            {product.name}<br/>
                            Price:{product.price}
                        </li>
                    );
                })}

            </ul>
        }else {
            return "There are no products in this category.";
        }
    }

    render() {
        /* Some css code has been removed for brevity */
        /* */

        //console.log("Main connect:",this.props);

        return (


            <div className="container">
                <div className="row">
                    <div className="col-3">
                        <Category></Category>
                    </div>
                    <div className="col-9">
                        <Link to="/">Home</Link><br/>

                        <Link to="/foo/sdfvsfd">Foo</Link><br/>

                        { this.renderProducts() }


                    </div>
                    <div>
                        <Pagination defaultCurrent={1}
                                    current={this.props.currentPage}
                                    total={this.props.products?this.props.products.length:0}
                                    onChange={this.props.onCurrentPageChange}
                                    style={{ margin: '100px' }} />
                    </div>

                </div>
            </div>

        );
    }
}

export default connect(
    state => ({
        products: state.products.productsList,
        currentPage: state.products.currentPage,
        productsOnPage: state.products.productsOnPage,
        mainStore: state
    }),
    dispatch => ({
        onCurrentPageChange:(current, pageSize)=>{
            //console.log("Sending UPDATE_CATEGORIES_LIST",list);
            dispatch({type: 'PAGE_CHANGED', payload: current});
        }
    })
)(App);


/* The if statement is required so as to Render the component on pages that have a div with an ID of "root";
*/


