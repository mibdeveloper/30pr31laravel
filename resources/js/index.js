

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import App from './components/App';
import Foo from './components/Foo';


// import { Router, Route, BrowserHistory  } from 'react-router-dom';
// import { syncHistoryWithStore} from 'react-router-redux';



import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import reducers from './reducers';
import * as c from './constants';



/*
const store = createStore(mainHandler, initialState
    /*window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() */

/*
function mainHandler(state = initialState, action){

    //console.log("Action", action);

    switch(action.type){
        case 'UPDATE_CATEGORIES_LIST':{
            return {...state, categories: [...action.payload]};
        }break;

        case 'UPDATE_PRODUCTS_LIST':{
            return {...state, products: [...action.payload.products], currentCategory:action.payload.categoryId, currentPage:1};
        }break;

        case 'PAGE_CHANGED':{
            return {...state, currentPage:action.payload};
        }break;

    }

    return state;
}
*/
const store = createStore(reducers);

// const history = syncHistoryWithStore(BrowserHistory, store);
// Add the reducer to your store on the `routing` key


/*    );*/
if (document.getElementById('root')) {
    ReactDOM.render(
        <Provider store={store}>
            <Router >
                <Switch>
                    <Route exact path="/" component={App} />
                    <Route path="/foo" component={Foo}/>
                </Switch>
            </Router>
        </Provider>


        , document.getElementById('root'));
}
