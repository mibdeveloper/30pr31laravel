<ul>
    @foreach($childs as $child)
    <li>
        <a href="{{ route('voyager.'.$dataType->slug.'.edit', ['shop_category' => $child->id]) }} ">{!!$child->name!!}</a>
        @can('add', app($dataType->model_name))
        <a href="{{ route('voyager.'.$dataType->slug.'.create-category', ['id' => $child->id]) }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
        @endcan

        @can('delete', app($dataType->model_name))

            <form style="display: inline" action="{{ route('voyager.'.$dataType->slug.'.destroy', ['shop_category' => $child->id]) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn btn-danger delete">Delete Category</button>
            </form>

        {{--<a href="" --}}
            {{--<i class="voyager-trash"></i> <span>{{ __('voyager::generic.delete') }}</span>--}}
        {{--</a>--}}
        @endcan


        @if(count($child->subcategories))
        @include('voyager::shop-categories.subcategory',
        ['childs'=>$child->subcategories])
        @endif
    </li>
    @endforeach
</ul>
