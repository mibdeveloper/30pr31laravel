<?php
/**
 * Created by PhpStorm.
 * User: Кизим.Анастасия
 * Date: 19.12.2018
 * Time: 19:55
 */

namespace App\Services\v1;

use App\ShopProduct;
class ProductService
{

    public function getProducts($parameters){


        if(isset($parameters["category"])){
            return ShopProduct::where('category_id', (int)$parameters["category"])->get();

        }

        return ShopProduct::with('categoryName')->get();

    }


}
