<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.01.2019
 * Time: 19:52
 *
 */

namespace App\Services\v1;

use \App\ShopCategory;
class CategoryService
{

    public function getCategories(){
//
  //      $products = ShopProduct::with('categoryName')->get();

        return ShopCategory::all();
    }

    public function getMainCategories(){
        return ShopCategory::where('parent_id', 0)->get();
    }


}
