<?php

namespace App\Providers\v1;

use App\Services\v1\ProductsService;
use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProductsService::class, function($app){
            return new ProductsService();
        });//
    }
}
