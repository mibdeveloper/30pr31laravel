<?php

namespace App\Http\Controllers;

use App\Http\Controllers\DB;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ShopProductsController extends VoyagerBaseController {
	public function index(Request $request) {
		$products = \DB::table('products')->get();
		$categories = \DB::table('categories')->get();
		return view('shop.products', compact('products', 'categories'));
	}

	public function showProductCard(Request $request) {
		$product = \DB::table('products')->find($request->id);
		return view('shop.product_card', compact('product'));
	}
}