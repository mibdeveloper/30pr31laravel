<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;
use App\ShopCategory;
use Log;

class ShopCategoriesController extends VoyagerBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $parentCategs = ShopCategory::where('parent_id', 0)->get();

        return view('voyager::shop-categories.browse',['dataType'=>$dataType, 'parentCategs' => $parentCategs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCategory(Request $request, $parentcatid)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();


        return view('voyager::shop-categories.edit-add', ['dataType'=>$dataType, 'parentId'=>$parentcatid]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       ///

        // проверяем, что это не ajax валидация
        if (!$request->ajax()){

            // сохраняем
            $m = new ShopCategory($request->all());
            $m->save();


            // логирование данных
            Log::info('Запись данных');
            Log::info(var_export($request->all(),true));

            // редирект  случае успеха. пепредаем сообщение, которое будет выводиться
            return redirect()->route('voyager.shop-categories.index',[])
                ->with('status', 'Категория '.$request->input("name").' добавлена');
        }




    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        //
        $dataTypeContent = ShopCategory::find($id);
        return view('voyager::shop-categories.edit-add',
            [
                'dataType'=>$dataType,
                'dataTypeContent'=>$dataTypeContent
            ]
            );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->ajax()){


            $m = ShopCategory::find($id);
            $name = $m->name = $request->input("name");
            $m->save();


            // логирование данных
            //Log::info('Запись данных');
            //Log::info(var_export($request->all(),true));

            // редирект  случае успеха. пепредаем сообщение, которое будет выводиться
            return redirect()->route('voyager.shop-categories.index',[])
               // ->with('status', 'Категория '.$name.' обновлена успешно');
               ->with([
                   'message'    => "Категория {$name} обновлена успешно",
                   'alert-type' => 'success',
               ]);
            /*
             * ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
             *
             * */
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        // проверяем, что это не ajax валидация
        if (!$request->ajax()){
            $m = ShopCategory::find($id);
            $name = $m->name;
            $m->delete();
            return redirect()->route('voyager.shop-categories.index',[])
                ->with('status', 'Категория "'.$name.'" удалена успешно');
        }
    }
}
