<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model
{
    public function categoryName()
    {
        return $this->hasOne('App\ShopCategory', 'id');
    }
}
